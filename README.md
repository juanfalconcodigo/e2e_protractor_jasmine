# Step 1

npm i

# Step 2

npx webdriver-manager update

# Step 3

npx webdriver-manager start

# Step 4

This will run in another cmd since the webdriver-manager must be initialized

npm run e2e

# Step 5

I need install allure-commandline -g and allure requires Java 8 or higher

npm install -g allure-commandline

> allure serve "path route report generate"

