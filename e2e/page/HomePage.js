const { element, browser } = require("protractor");

const HomePage = function() {
    let firstNumber = element(by.model('first'));
    let secondNumber = element(by.model('second'));
    let operator = element(by.model('second'));
    let button = element(by.id('gobutton'));
    let result = element(by.css('h2'));
    let selectOperationAddition = element(by.xpath(".//option[@value='ADDITION']"));
    let selectOperationSubstraction = element(by.xpath(".//option[@value='SUBTRACTION']"));
    let selectOperationMultiplication = element(by.xpath(".//option[@value='MULTIPLICATION']"));
    let selectOperationDivision = element(by.xpath(".//option[@value='DIVISION']"));
    let selectOperationModulo = element(by.xpath(".//option[@value='MODULO']"));

    this.get = async function() {
        await browser.get('http://juliemr.github.io/protractor-demo/');
    }

    this.setFirstNumber = async function(value) {
        await firstNumber.sendKeys(value);
    }

    this.setSecondNumber = async function(value) {
        await secondNumber.sendKeys(value);
    }

    this.operatorIcon = async function() {
        await operator.click();
    }

    this.doAddition = async function() {
        await button.click();
    }

    this.getResult = async function() {
        return await result.getText();
    }
    this.selectOperation = async function(operation) {
        switch (operation) {
            case 'ADDITION':
                await selectOperationAddition.click()
                break;
            case 'SUBTRACTION':
                await selectOperationSubstraction.click()
                break;
            case 'MULTIPLICATION':
                await selectOperationMultiplication.click()
                break;
            case 'DIVISION':
                await selectOperationDivision.click()
                break;
            case 'MODULO':
                await selectOperationModulo.click()
                break;
            default:
                await selectOperationAddition.click()
                break;
        }
    }
}

module.exports = new HomePage();