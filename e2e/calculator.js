const { browser } = require("protractor");
const homePage = require('./page/HomePage');

// spec.js
describe('Protractor Demo App', function() {
    it('It should have a title', async function() {
        await browser.get('http://juliemr.github.io/protractor-demo/');

        expect(browser.getTitle()).toEqual('Super Calculator');
        await browser.sleep(2000);
    });

    it('It should return result sum 4', async() => {
        await homePage.get();
        await homePage.setFirstNumber('1');
        await homePage.setSecondNumber('3');
        await homePage.operatorIcon();
        await homePage.selectOperation();
        await homePage.doAddition();
        expect(homePage.getResult()).toBe('4');
        await browser.sleep(5000);
    });

    it('It should return result subtract 5', async() => {
        await homePage.get();
        await homePage.setFirstNumber('15');
        await homePage.setSecondNumber('10');
        await homePage.operatorIcon();
        await homePage.selectOperation('SUBTRACTION');
        await homePage.doAddition();
        expect(homePage.getResult()).toBe('5');
        await browser.sleep(5000);
    });

    it('It should return result multiply 24', async() => {
        await homePage.get();
        await homePage.setFirstNumber('6');
        await homePage.setSecondNumber('4');
        await homePage.operatorIcon();
        await homePage.selectOperation('MULTIPLICATION');
        await homePage.doAddition();
        expect(homePage.getResult()).toBe('24');
        await browser.sleep(5000);
    });

    it('It should return result divide 3', async() => {
        await homePage.get();
        await homePage.setFirstNumber('27');
        await homePage.setSecondNumber('9');
        await homePage.operatorIcon();
        await homePage.selectOperation('DIVISION');
        await homePage.doAddition();
        expect(homePage.getResult()).toBe('3');
        await browser.sleep(5000);
    });

    it('It should return result mod  2', async() => {
        await homePage.get();
        await homePage.setFirstNumber('10');
        await homePage.setSecondNumber('4');
        await homePage.operatorIcon();
        await homePage.selectOperation('MODULO');
        await homePage.doAddition();
        expect(homePage.getResult()).toBe('2');
        await browser.sleep(5000);
    });
});